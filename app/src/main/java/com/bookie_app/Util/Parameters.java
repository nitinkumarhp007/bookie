package com.bookie_app.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "Authorization-Key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String Q = "q";
    public static final String SOCIAL_TOKEN = "social_token";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String TAG_ID = "tag_id";
    public static final String FIRST_NAME = "first_name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String LOCATION = "location";
    public static final String COUNTRY_CODE = "country_code";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String IMAGE = "image";
    public static final String START_TIME_CALL = "start_time_call";
    public static final String END_TIME_CALL = "end_time_call";
    public static final String CENTRE = "centre";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String USERNAME = "username";
    public static final String DOB = "dob";
    public static final String ADDRESS = "address";

    public static final String MESSAGE = "message";
    public static final String CLASS_ID = "class_id";

    public static final String FRIEND_ID = "friend_id";
    public static final String TITLE = "title";
    public static final String LATITUDE ="latitude" ;
    public static final String LONGITUDE ="longitude" ;
    public static final String REQUEST_ID = "request_id";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String ACCEPTANCE_END = "acceptance_end";
    public static final String DATE_TIME_TYPE = "date_time_type";
    public static final String WITNESS_NEED = "witness_need";
    public static final String WAGER_TYPE = "wager_type";
    public static final String WAGER = "wager";
    public static final String PARTICIPANTS = "participants";
    public static final String PARTICIPANT_ID = "Participant_id";
    public static final String DESCRIPTION = "description";
    public static final String PRO_CONTRA = "pro_contra";
    public static final String TIMEFRAME = "timeframe";
    public static final String USERS = "users";
    public static final String STATUS = "status";
    public static final String DATE = "date";
    public static final String TIME ="time" ;
    public static final String PAYMENT_METHOD = "payment_method";
}















