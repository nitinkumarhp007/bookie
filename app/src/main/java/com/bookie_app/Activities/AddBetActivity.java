package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddBetActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    AddBetActivity context;
    @BindView(R.id.name_of_bet)
    EditText nameOfBet;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.acceptance_end)
    TextView acceptanceEnd;
    @BindView(R.id.participants)
    TextView participants;
    @BindView(R.id.pro_contra)
    EditText proContra;
    @BindView(R.id.start_time_date)
    RadioButton startTimeDate;
    @BindView(R.id.timeframe)
    RadioButton timeframe;
    @BindView(R.id.timeframe_text)
    EditText timeframeText;
    @BindView(R.id.timeframe_layout)
    LinearLayout timeframeLayout;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.date_layout)
    LinearLayout dateLayout;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.time_layout)
    LinearLayout timeLayout;
    @BindView(R.id.yes)
    RadioButton yes;
    @BindView(R.id.no)
    RadioButton no;
    @BindView(R.id.wager_type)
    TextView wagerType;
    @BindView(R.id.wager)
    EditText wager;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.wager_layout)
    LinearLayout wagerLayout;

    private SavePref savePref;

    String wager_type = "";

    String friend_ids = "";

    EventBetModel eventBetModel;

    boolean is_from_my = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bet);
        ButterKnife.bind(this);

        context = AddBetActivity.this;
        savePref = new SavePref(context);


        is_from_my = getIntent().getBooleanExtra("is_from_my", false);

        if (is_from_my) {
            eventBetModel = getIntent().getExtras().getParcelable("data");

            nameOfBet.setText(eventBetModel.getName());
            description.setText(eventBetModel.getDescription());
            acceptanceEnd.setText(eventBetModel.getAcceptance_end());
            proContra.setText(eventBetModel.getPro_contra());

            if (eventBetModel.getDate_time_type().equals("1")) {
                startTimeDate.setChecked(true);
                timeframe.setChecked(false);

                date.setText(util.convertTimeStampDate(Long.parseLong(eventBetModel.getDate())));
                time.setText(util.convertTimeStampTime(Long.parseLong(eventBetModel.getTime())));

                dateLayout.setVisibility(View.VISIBLE);
                timeLayout.setVisibility(View.VISIBLE);
                timeframeLayout.setVisibility(View.GONE);


            } else {
                timeframe.setChecked(true);
                startTimeDate.setChecked(false);
                timeframeText.setText(eventBetModel.getTimeframe());

                dateLayout.setVisibility(View.GONE);
                timeLayout.setVisibility(View.GONE);
                timeframeLayout.setVisibility(View.VISIBLE);
            }

            if (eventBetModel.getWitness_need().equals("1")) {
                yes.setChecked(true);
                no.setChecked(false);
            } else {
                yes.setChecked(false);
                no.setChecked(true);
            }

            String users = "";
            if (eventBetModel != null) {
                for (int i = 0; i < eventBetModel.getList().size(); i++) {

                    if (users.isEmpty()) {
                        users = eventBetModel.getList().get(i).getName();
                    } else {
                        users = users + "," + eventBetModel.getList().get(i).getName();
                    }
                }
            }


            participants.setText(users);


            if (eventBetModel.getWager_type().equals("1")) {
                wager_type = "1";
                wagerType.setText("Money");
                wager.setText("$" + eventBetModel.getWager());
                wagerLayout.setVisibility(View.VISIBLE);
            } else if (eventBetModel.getWager_type().equals("2")) {
                wager_type = "2";
                wagerType.setText("Things");
                wager.setText(eventBetModel.getWager() + " Things(s)");
                wagerLayout.setVisibility(View.VISIBLE);
            } else if (eventBetModel.getWager_type().equals("3")) {
                wager_type = "3";
                wagerType.setText("Fame and Honor");
                wagerLayout.setVisibility(View.GONE);
            }


            nameOfBet.setEnabled(false);
            description.setEnabled(false);
            acceptanceEnd.setEnabled(false);
            participants.setEnabled(false);
            proContra.setEnabled(false);
            startTimeDate.setEnabled(false);
            timeframe.setEnabled(false);
            date.setEnabled(false);
            time.setEnabled(false);
            timeframeText.setEnabled(false);
            yes.setEnabled(false);
            no.setEnabled(false);
            wagerType.setEnabled(false);
            wager.setEnabled(false);

            submit.setVisibility(View.GONE);


            //participants.setText(eventBetModel.getAcceptance_end());
        }

        startTimeDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dateLayout.setVisibility(View.VISIBLE);
                    timeLayout.setVisibility(View.VISIBLE);
                    timeframeLayout.setVisibility(View.GONE);
                }
            }
        });
        timeframe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dateLayout.setVisibility(View.GONE);
                    timeLayout.setVisibility(View.GONE);
                    timeframeLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_from_my) {
            title.setText(getResources().getString(R.string.add_bet));
        } else {
            title.setText("Bet Detail");
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private void AddBetTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (nameOfBet.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (acceptanceEnd.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please select Date of Acceptance End");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (participants.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Participants");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (proContra.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Pro Contra");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (wagerType.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Wager Type");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }  else {
                if (startTimeDate.isChecked()) {
                    if (date.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Select Date");
                        submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (time.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Select Time");
                        submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        if(wager_type.equals("1") || wager_type.equals("2"))
                        {
                            if (wager.getText().toString().trim().isEmpty()) {
                                util.IOSDialog(context, "Please Enter Wager");
                                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            }
                            else {
                                if (wager_type.equals("1")) {
                                    go_to_checkout();
                                } else {
                                    ADD_BAT_API();
                                }
                            }
                        }
                        else {
                            ADD_BAT_API();
                        }
                    }
                } else if (timeframe.isChecked()) {
                    if (timeframe.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter TimeFrame");
                        submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {


                        if(wager_type.equals("1") || wager_type.equals("2"))
                        {
                            if (wager.getText().toString().trim().isEmpty()) {
                                util.IOSDialog(context, "Please Enter Wager");
                                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            }
                            else {
                                if (wager_type.equals("1")) {
                                    go_to_checkout();
                                } else {
                                    ADD_BAT_API();
                                }
                            }
                        }
                        else {
                            ADD_BAT_API();
                        }


                    }
                }
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void go_to_checkout() {
        String date_time_type = "", witness_need = "";
        if (startTimeDate.isChecked()) {
            date_time_type = "1";
        } else {
            date_time_type = "2";
        }
        if (yes.isChecked()) {
            witness_need = "1";
        } else {
            witness_need = "0";
        }

        Intent intent = new Intent(context, CheckOutActivity.class);
        intent.putExtra("NAME", nameOfBet.getText().toString().trim());
        intent.putExtra("DESCRIPTION", description.getText().toString().trim());
        intent.putExtra("ACCEPTANCE_END", acceptanceEnd.getText().toString().trim());
        intent.putExtra("PARTICIPANTS", friend_ids);
        intent.putExtra("PRO_CONTRA", proContra.getText().toString().trim());
        intent.putExtra("DATE_TIME_TYPE", date_time_type);
        if (!date.getText().toString().trim().isEmpty())
            intent.putExtra("DATE", util.date_to_timestamp(date.getText().toString().trim()));
        if (!time.getText().toString().trim().isEmpty())
            intent.putExtra("TIME", util.time_to_timestamp(time.getText().toString().trim()));
        intent.putExtra("TIMEFRAME", timeframeText.getText().toString().trim());
        intent.putExtra("WITNESS_NEED", witness_need);
        intent.putExtra("WAGER", wager.getText().toString().trim());
        intent.putExtra("WAGER_TYPE", wager_type);
        startActivity(intent);
    }

    private void ADD_BAT_API() {
        String date_time_type = "", witness_need = "";
        if (startTimeDate.isChecked()) {
            date_time_type = "1";
        } else {
            date_time_type = "2";
        }
        if (yes.isChecked()) {
            witness_need = "1";
        } else {
            witness_need = "0";
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, nameOfBet.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ACCEPTANCE_END, acceptanceEnd.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PARTICIPANTS, friend_ids);
        formBuilder.addFormDataPart(Parameters.PRO_CONTRA, proContra.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DATE_TIME_TYPE, date_time_type);
        if (date_time_type.equals("1")) {
            formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date.getText().toString().trim()));
            formBuilder.addFormDataPart(Parameters.TIME, util.time_to_timestamp(time.getText().toString().trim()));
        } else {
            formBuilder.addFormDataPart(Parameters.TIMEFRAME, timeframeText.getText().toString().trim());
        }

        formBuilder.addFormDataPart(Parameters.WITNESS_NEED, witness_need);
        formBuilder.addFormDataPart(Parameters.WAGER, wager.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.WAGER_TYPE, wager_type);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_BAT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Bet Added successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.acceptance_end, R.id.participants, R.id.date_layout, R.id.time_layout, R.id.wager_type, R.id.submit})
    public void onClick(View view) {
        util.hideKeyboard(context);
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        switch (view.getId()) {
            case R.id.acceptance_end:
                if (!is_from_my) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    acceptanceEnd.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }

                break;
            case R.id.participants:

                if (!is_from_my) {
                    add_user();
                }
                break;
            case R.id.date_layout:
                if (!is_from_my) {
                    DatePickerDialog datePickerDialog1 = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog1.show();

                }
                break;
            case R.id.time_layout:
                if (!is_from_my) {
                    // Get Current Time
                    final Calendar c11 = Calendar.getInstance();
                    int mHour = c11.get(Calendar.HOUR_OF_DAY);
                    int mMinute = c11.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    time.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, true);
                    timePickerDialog.show();
                }

                break;
            case R.id.wager_type:
                if (!is_from_my) {
                    Show_WagerType();
                }
                break;
            case R.id.submit:

                AddBetTask();
                break;
        }
    }

    private void add_user() {
        CharSequence[] items = {"Choose From Phone Book", "Search by Name"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    startActivityForResult(new Intent(context, UserListActivity.class), 200);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else if (item == 1) {
                    startActivityForResult(new Intent(context, SearchUsersActivity.class), 200);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void Show_WagerType() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Wager Type");

        /*ArrayList to Array Conversion */
        String array[] = new String[3];
        array[0] = "Money";
        array[1] = "Things";
        array[2] = "Fame and Honor";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    wager_type = "1";
                    wagerType.setText("Money");
                    wagerLayout.setVisibility(View.VISIBLE);
                } else if (which == 1) {
                    wager_type = "2";
                    wagerLayout.setVisibility(View.VISIBLE);
                    wagerType.setText("Things");
                } else if (which == 2) {
                    wager_type = "3";
                    wagerType.setText("Fame and Honor");
                    wagerLayout.setVisibility(View.GONE);
                }
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //    String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        //dateTextView.setText(date);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //String time = "You picked the following time: " + hourOfDay + "h" + minute + "m" + second;
        //timeTextView.setText(time);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200 && resultCode == RESULT_OK) {
            String friend_names = "";
            friend_ids = "";
            if (util.StoreContacts.size() > 0) {
                for (int i = 0; i < util.StoreContacts.size(); i++) {
                    if (friend_ids.isEmpty()) {
                        friend_ids = util.StoreContacts.get(i).getId();
                        friend_names = util.StoreContacts.get(i).getName();
                    } else {
                        friend_ids = friend_ids + "," + util.StoreContacts.get(i).getId();
                        friend_names = friend_names + "," + util.StoreContacts.get(i).getName();
                    }
                }
                participants.setText(friend_names);
            }
        }
    }
}
