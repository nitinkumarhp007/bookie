package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity {
    SignUpActivity context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.accept_the_terms_amp_conditions)
    CheckBox acceptTheTermsAmpConditions;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.sign_in)
    Button signIn;
    Uri fileUri;
    private String selectedimage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        context=SignUpActivity.this;
    }

    @OnClick({R.id.profile_pic, R.id.sign_up, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                CropImage.activity(fileUri)
                        .setAspectRatio(2,2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.sign_up:
                SignUpProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SignUpProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your Name");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (confirmPassword.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Confirm Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                util.IOSDialog(context, "Password Not Match");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }  else if (!acceptTheTermsAmpConditions.isChecked()) {
                util.IOSDialog(context, "Please Accept Terms & Conditions");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                USER_SIGNUP_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.FIRST_NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());

        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("authorization_key"));
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "User Registered Sucessfully!!!");

                        } else {
                            signUp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK)
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(profilePic);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }
    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
