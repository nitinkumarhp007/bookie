package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {
    SignInActivity context;
    private SavePref savePref;
    @BindView(R.id.email)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.sign_up)
    Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        context=SignInActivity.this;
        savePref=new SavePref(context);
    }

    @OnClick({R.id.forgot_password, R.id.sign_in, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignUpActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        Log.e("device_token__", SavePref.getDeviceToken(SignInActivity.this, "token"));
        String s = SavePref.getDeviceToken(SignInActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("first_name"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("image"));
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
