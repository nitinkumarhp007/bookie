package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.ligl.android.widget.iosdialog.IOSSheetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddEventActivity extends AppCompatActivity {
    AddEventActivity context;
    @BindView(R.id.name_of_event)
    EditText nameOfEvent;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.acceptance_end)
    TextView acceptanceEnd;
    @BindView(R.id.participants)
    TextView participants;
    @BindView(R.id.date_time)
    TextView dateTime;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.date_layout)
    LinearLayout dateLayout;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.time_layout)
    LinearLayout timeLayout;
    @BindView(R.id.pro_contra)
    EditText proContra;
    @BindView(R.id.wager_type)
    TextView wagerType;
    @BindView(R.id.wager)
    EditText wager;
    @BindView(R.id.submit)
    Button submit;
    private SavePref savePref;

    String date_time_type = "";
    String wager_type = "", friend_ids = "";

    EventBetModel eventBetModel;

    boolean is_from_my = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        ButterKnife.bind(this);
        context = AddEventActivity.this;
        savePref = new SavePref(context);

        is_from_my = getIntent().getBooleanExtra("is_from_my", false);

        if (is_from_my) {
            eventBetModel = getIntent().getExtras().getParcelable("data");

            nameOfEvent.setText(eventBetModel.getName());
            description.setText(eventBetModel.getDescription());
            acceptanceEnd.setText(eventBetModel.getAcceptance_end());
            proContra.setText(eventBetModel.getPro_contra());

            date.setText(util.convertTimeStampDate(Long.parseLong(eventBetModel.getDate())));
            time.setText(util.convertTimeStampTime(Long.parseLong(eventBetModel.getTime())));

            if (eventBetModel.getWager_type().equals("1")) {
                wager_type = "1";
                wagerType.setText("Cash");
                wager.setText("$" + eventBetModel.getWager());
            } else if (eventBetModel.getWager_type().equals("2")) {
                wager_type = "2";
                wagerType.setText("Beer");
                wager.setText(eventBetModel.getWager() + " Beer(s)");
            }


            if (eventBetModel.getDate_time_type().equals("1")) {
                date_time_type = "1";
                dateTime.setText("Not later than date/time");
            } else if (eventBetModel.getDate_time_type().equals("1")) {
                date_time_type = "2";
                dateTime.setText("On a specific date");
            } else if (eventBetModel.getDate_time_type().equals("1")) {
                date_time_type = "3";
                dateTime.setText("Earliest by date/time");
            }

            String users = "";
            for (int i = 0; i < eventBetModel.getList().size(); i++) {

                if (users.isEmpty()) {
                    users = eventBetModel.getList().get(i).getName();
                } else {
                    users = users + "," + eventBetModel.getList().get(i).getName();
                }
            }

            participants.setText(users);



            nameOfEvent.setEnabled(false);
            description.setEnabled(false);
            acceptanceEnd.setEnabled(false);
            participants.setEnabled(false);
            proContra.setEnabled(false);
            date.setEnabled(false);
            time.setEnabled(false);
            wagerType.setEnabled(false);
            wager.setEnabled(false);
            dateTime.setEnabled(false);

            submit.setVisibility(View.GONE);
        }

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (!is_from_my) {
            title.setText(getResources().getString(R.string.add_event));
        } else {
            title.setText("Event Detail");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.acceptance_end, R.id.participants, R.id.date_time, R.id.date_layout, R.id.time_layout, R.id.wager_type, R.id.submit})
    public void onClick(View view) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        switch (view.getId()) {
            case R.id.acceptance_end:
                if (!is_from_my) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    acceptanceEnd.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
                break;
            case R.id.participants:
                if (!is_from_my) {
                    startActivityForResult(new Intent(this, UserListActivity.class), 200);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                break;
            case R.id.date_time:
                if (!is_from_my) {
                    IOSSheetDialog.SheetItem[] items2 = new IOSSheetDialog.SheetItem[3];
                    items2[0] = new IOSSheetDialog.SheetItem("Not later than date/time", IOSSheetDialog.SheetItem.BLUE);
                    items2[1] = new IOSSheetDialog.SheetItem("On a specific date", IOSSheetDialog.SheetItem.BLUE);
                    items2[2] = new IOSSheetDialog.SheetItem("Earliest by date/time", IOSSheetDialog.SheetItem.BLUE);
                    IOSSheetDialog dialog22 = new IOSSheetDialog.Builder(this)
                            .setTitle("When does the Event Arrive").setData(items2, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        date_time_type = "1";
                                        dateTime.setText("Not later than date/time");
                                    } else if (which == 1) {
                                        date_time_type = "2";
                                        dateTime.setText("On a specific date");
                                    } else {
                                        date_time_type = "3";
                                        dateTime.setText("Earliest by date/time");
                                    }
                                }
                            }).show();
                }

                break;
            case R.id.date_layout:
                if (!is_from_my) {
                    DatePickerDialog datePickerDialog1 = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog1.show();
                }
                break;
            case R.id.time_layout:
                if (!is_from_my) {
                    // Get Current Time
                    final Calendar c11 = Calendar.getInstance();
                    int mHour = c11.get(Calendar.HOUR_OF_DAY);
                    int mMinute = c11.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    time.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, true);
                    timePickerDialog.show();
                }
                break;
            case R.id.wager_type:
                if (!is_from_my) {
                    IOSSheetDialog.SheetItem[] items222 = new IOSSheetDialog.SheetItem[2];
                    items222[0] = new IOSSheetDialog.SheetItem("Cash", IOSSheetDialog.SheetItem.BLUE);
                    items222[1] = new IOSSheetDialog.SheetItem("Beer", IOSSheetDialog.SheetItem.BLUE);
                    IOSSheetDialog dialog222 = new IOSSheetDialog.Builder(this)
                            .setTitle("Wager Type").setData(items222, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        wager_type = "1";
                                        wagerType.setText("Cash");
                                    } else if (which == 1) {
                                        wager_type = "2";
                                        wagerType.setText("Beer");
                                    }
                                }
                            }).show();
                }
                break;
            case R.id.submit:
                AddEventTask();
                break;
        }
    }

    private void AddEventTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (nameOfEvent.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (acceptanceEnd.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please select Date of Acceptance End");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (participants.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Participants");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (dateTime.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select When does the event arrive");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (date.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Date");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (time.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Time");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (proContra.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Pro Contra");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (wagerType.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Wager Type");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (wager.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Wager");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                ADD_EVENT_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void ADD_EVENT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, nameOfEvent.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ACCEPTANCE_END, acceptanceEnd.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PARTICIPANTS, friend_ids);
        formBuilder.addFormDataPart(Parameters.PRO_CONTRA, proContra.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DATE_TIME_TYPE, date_time_type);
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date.getText().toString().trim()));
        formBuilder.addFormDataPart(Parameters.TIME, util.time_to_timestamp(time.getText().toString().trim()));
        formBuilder.addFormDataPart(Parameters.WAGER, wager.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.WAGER_TYPE, wager_type);
        formBuilder.addFormDataPart(Parameters.WITNESS_NEED, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_EVENT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Event Added Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200 && resultCode == RESULT_OK) {
            friend_ids = data.getStringExtra("friend_ids");
            String friend_names = data.getStringExtra("friend_names");

            participants.setText(friend_names);
        }
    }
}
