package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CheckOutActivity extends AppCompatActivity {
    CheckOutActivity context;
    @BindView(R.id.paypal)
    RelativeLayout paypal;
    @BindView(R.id.cash)
    RelativeLayout cash;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);
        context = CheckOutActivity.this;
        savePref = new SavePref(context);
        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Payment Method");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.paypal, R.id.cash})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.paypal:
                paypal.setBackgroundColor(getResources().getColor(R.color.light_gray));
                Alert("1");
                break;
            case R.id.cash:
                cash.setBackgroundColor(getResources().getColor(R.color.light_gray));
                Alert("2");
                break;
        }
    }

    private void Alert(String payment_method) {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ADD_BAT_API(payment_method);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        paypal.setBackgroundColor(getResources().getColor(R.color.white));
                        cash.setBackgroundColor(getResources().getColor(R.color.white));
                        dialog.dismiss();
                    }
                }).show();
    }


    private void ADD_BAT_API(String payment_method) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, getIntent().getStringExtra("NAME"));
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, getIntent().getStringExtra("DESCRIPTION"));
        formBuilder.addFormDataPart(Parameters.ACCEPTANCE_END, getIntent().getStringExtra("ACCEPTANCE_END"));
        formBuilder.addFormDataPart(Parameters.PARTICIPANTS, getIntent().getStringExtra("PARTICIPANTS"));
        formBuilder.addFormDataPart(Parameters.PRO_CONTRA, getIntent().getStringExtra("PRO_CONTRA"));
        formBuilder.addFormDataPart(Parameters.DATE_TIME_TYPE, getIntent().getStringExtra("DATE_TIME_TYPE"));
        if (getIntent().getStringExtra("DATE_TIME_TYPE").equals("1")) {
            formBuilder.addFormDataPart(Parameters.DATE, getIntent().getStringExtra("DATE"));
            formBuilder.addFormDataPart(Parameters.TIME, getIntent().getStringExtra("TIME"));
        } else {
            formBuilder.addFormDataPart(Parameters.TIMEFRAME, getIntent().getStringExtra("TIMEFRAME"));
        }

        formBuilder.addFormDataPart(Parameters.WITNESS_NEED, getIntent().getStringExtra("WITNESS_NEED"));
        formBuilder.addFormDataPart(Parameters.WAGER, getIntent().getStringExtra("WAGER"));
        formBuilder.addFormDataPart(Parameters.WAGER_TYPE, getIntent().getStringExtra("WAGER_TYPE"));
        formBuilder.addFormDataPart(Parameters.PAYMENT_METHOD, payment_method);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_BAT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Bet Added successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
