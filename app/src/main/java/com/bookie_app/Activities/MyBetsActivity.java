package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.Adapters.MyBetsAdapter;
import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.DataModels.UserModel;
import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyBetsActivity extends AppCompatActivity {
    MyBetsActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_text)
    TextView errorText;
    private SavePref savePref;
    private ArrayList<EventBetModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bets);
        ButterKnife.bind(this);

        context = MyBetsActivity.this;
        savePref = new SavePref(context);
        setToolbar();


        if (ConnectivityReceiver.isConnected()) {
            MY_BATS_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


    }

    private void MY_BATS_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.MY_BATS, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i).getJSONObject("object");
                                EventBetModel eventBetModel = new EventBetModel();

                                eventBetModel.setAcceptance_end(object.getString("acceptance_end"));
                                eventBetModel.setCreated(object.getString("created"));
                                eventBetModel.setDate(object.getString("date"));
                                eventBetModel.setDate_time_type(object.getString("date_time_type"));
                                eventBetModel.setDescription(object.getString("description"));
                                eventBetModel.setId(object.getString("id"));
                                eventBetModel.setName(object.getString("name"));
                                eventBetModel.setParticipants(object.getString("participants"));
                                eventBetModel.setPro_contra(object.getString("pro_contra"));
                                eventBetModel.setTime(object.getString("time"));
                                eventBetModel.setTimeframe(object.getString("timeframe"));
                                eventBetModel.setType(object.getString("type"));
                                eventBetModel.setWager(object.getString("wager"));
                                eventBetModel.setWager_type(object.getString("wager_type"));
                                eventBetModel.setWitness_need(object.getString("witness_need"));

                                JSONArray Users = data.getJSONObject(i).getJSONArray("Users");
                                ArrayList<UserModel> list_users = new ArrayList<>();
                                for (int j = 0; j < Users.length(); j++) {
                                    JSONObject obj = Users.getJSONObject(j).getJSONObject("User");
                                    UserModel userModel = new UserModel();
                                    userModel.setId(obj.getString("id"));
                                    userModel.setName(obj.getString("first_name"));
                                    userModel.setProfile(obj.getString("image"));
                                    userModel.setPhone(obj.getString("phone"));
                                    list_users.add(userModel);
                                }
                                eventBetModel.setList(list_users);

                                list.add(eventBetModel);

                            }
                            if (list.size() > 0) {
                                myRecyclerView.setVisibility(View.VISIBLE);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(new MyBetsAdapter(context, list));
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.my_bets));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
