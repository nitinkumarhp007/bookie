package com.bookie_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {
    ChangePasswordActivity context;
    @BindView(R.id.current_password)
    EditText currentPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.change)
    Button change;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);


        context = ChangePasswordActivity.this;
        savePref = new SavePref(context);
        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.change_password));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.change)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (currentPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Current Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (newPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter New Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (confirmPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Confirm Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                util.IOSDialog(context, "Password not match");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                CHANGE_PASSWORD_API();
            }
        }
    }

    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OLD_PASSWORD, currentPassword.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NEW_PASSWORD, newPassword.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHANGEPASSWORD, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
