package com.bookie_app.parser;


public class AllAPIS {

    public static final String BASE_URL = "http://52.15.68.129/bookie/apis/";

    public static final String USERLOGIN = BASE_URL + "UserLogin";
    public static final String USER_SIGNUP = BASE_URL + "user_signup";
    public static final String VERIFY_OTP = BASE_URL + "VerifyOtp";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "edit_profile";

    public static final String ADD_BAT = BASE_URL + "add_bat";
    public static final String ADD_EVENT = BASE_URL + "add_event";
    public static final String CHECK_PHONEBOOK = BASE_URL + "check_phonebook";
    public static final String MY_EVENTS = BASE_URL + "my_events";
    public static final String MY_BATS = BASE_URL + "my_bats";
    public static final String REQUEST_BATS_LISTING = BASE_URL + "request_bats_listing";
    public static final String REQUEST_STATUS = BASE_URL + "request_status";
    public static final String NOTIFICATIONS = BASE_URL + "get_all_notification";
    public static final String USER_LISTING = BASE_URL + "User_listing";

}
