package com.bookie_app.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message = "", created = "", image, notification_code = "", otheruserid, username = "", sender_id = "", order_id, timeStamp;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());
        message = remoteMessage.getData().get("message");
        notification_code = remoteMessage.getData().get("notification_code");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }

        try {
            JSONObject object = new JSONObject(remoteMessage.getData().get("body"));
            message = object.getJSONObject("data").getString("message");
            created = object.getJSONObject("data").getString("created");
            sender_id = object.getJSONObject("data").getString("sender_id");
            order_id = object.getJSONObject("data").getString("order_id");
            username = object.getJSONObject("data").optString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (savePref.getChatScreen() && savePref.getCHAT_ID().equals(sender_id)) {
            publishResultsMessage(message, created, username, sender_id);
        } else {
            sendNotification(getApplicationContext(), message);
        }

    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("token__", token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
    }

    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;
        intent = new Intent(context, MainActivity.class);
        if (notification_code.equals("1")) {

            intent.putExtra("new_request_bet", true);
        }

        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

    /*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    private void publishResultsMessage(String message, String created, String username, String sender_id) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("sender_id", sender_id);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
