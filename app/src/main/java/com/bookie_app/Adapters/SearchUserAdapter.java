package com.bookie_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.Activities.SearchUsersActivity;
import com.bookie_app.DataModels.ContactsList;
import com.bookie_app.DataModels.UserModel;
import com.bookie_app.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.RecyclerViewHolder> {
    SearchUsersActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ContactsList> list;

    public SearchUserAdapter(SearchUsersActivity context, ArrayList<ContactsList> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.serachuser_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.phone.setText(list.get(position).getPhone());
        Glide.with(context).load(list.get(position).getProfile()).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.AddButtonTask(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /*public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ContactsList> nList = new ArrayList<ContactsList>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ContactsList wp : tempList) {
                String s = wp.getName().toLowerCase() + " " + wp.getPhone();
                if (s.contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        storeContacts = nList;
        notifyDataSetChanged();
    }*/
}
