package com.bookie_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.Activities.AddBetActivity;
import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.R;
import com.bookie_app.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class MyBetsAdapter extends RecyclerView.Adapter<MyBetsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<EventBetModel> list;

    public MyBetsAdapter(Context context, ArrayList<EventBetModel> list) {
        this.list = list;
        this.context = context;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.my_bets_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.description.setText(list.get(position).getDescription());

        if (list.get(position).getDate_time_type().equals("1")) {
            holder.dateTime.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())) +
                    "  " + util.convertTimeStampTime(Long.parseLong(list.get(position).getTime())));
        } else {
            holder.dateTime.setText(list.get(position).getTimeframe() + " min.");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddBetActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("is_from_my",true);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date_time)
        TextView dateTime;
        @BindView(R.id.description)
        TextView description;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
