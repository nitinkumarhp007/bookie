package com.bookie_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.Activities.AddBetActivity;
import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.Fragments.HomeFragment;
import com.bookie_app.R;
import com.bookie_app.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    HomeFragment homeFragment;
    private View view;
    ArrayList<EventBetModel> list;

    public RequestAdapter(Context context, ArrayList<EventBetModel> list, HomeFragment homeFragment) {
        this.context = context;
        this.list = list;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.requst_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.description.setText(list.get(position).getDescription());
        if (list.get(position).getDate_time_type().equals("1")) {
            holder.date.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())) +
                    "  " + util.convertTimeStampTime(Long.parseLong(list.get(position).getTime())));
        } else {
            holder.date.setText(list.get(position).getTimeframe() + " min.");
        }

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.REQUEST_STATUS(position,"1","1");
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.REQUEST_STATUS(position,"2","1");
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddBetActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("is_from_my",true);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.created_by)
        TextView createdBy;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.accept)
        TextView accept;
        @BindView(R.id.decline)
        TextView decline;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
