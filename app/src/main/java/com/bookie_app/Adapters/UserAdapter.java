package com.bookie_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.DataModels.ContactsList;
import com.bookie_app.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ContactsList> tempList;
    ArrayList<ContactsList> storeContacts;

    public UserAdapter(Context context, ArrayList<ContactsList> storeContacts) {
        this.context = context;
        this.tempList = storeContacts;
        this.storeContacts = storeContacts;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(storeContacts.get(position).getName());
        holder.phone.setText(storeContacts.get(position).getPhone());
        Glide.with(context).load(storeContacts.get(position).getProfile()).into(holder.image);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    storeContacts.get(position).setCheck(true);
                else
                    storeContacts.get(position).setCheck(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return storeContacts.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.check_box)
        CheckBox checkBox;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ContactsList> nList = new ArrayList<ContactsList>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ContactsList wp : tempList) {
                String s = wp.getName().toLowerCase() + " " + wp.getPhone();
                if (s.contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        storeContacts = nList;
        notifyDataSetChanged();
    }
}
