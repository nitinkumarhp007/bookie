package com.bookie_app.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bookie_app.Activities.ChangePasswordActivity;
import com.bookie_app.Activities.MyBetsActivity;
import com.bookie_app.Activities.MyEventsActivity;
import com.bookie_app.Activities.SignInActivity;
import com.bookie_app.Activities.UpdateProfileActivity;
import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    Context context;
    private SavePref savePref;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.invite_a_friend)
    RelativeLayout invite_a_friend;
    @BindView(R.id.my_bets)
    RelativeLayout myBets;
    @BindView(R.id.edit_profile)
    RelativeLayout editProfile;
    @BindView(R.id.change_password)
    RelativeLayout changePassword;
    @BindView(R.id.privacy_policy)
    RelativeLayout privacyPolicy;
    @BindView(R.id.logout)
    RelativeLayout logout;

    public ProfileFragment() {
        // Required empty public constructor
    }


    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        Glide.with(context).load(savePref.getImage()).error(R.drawable.place_holder).into(profilePic);
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.invite_a_friend, R.id.my_bets, R.id.edit_profile, R.id.change_password, R.id.privacy_policy, R.id.logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.invite_a_friend:
                sharepost();
                break;
            case R.id.my_bets:
                startActivity(new Intent(context, MyBetsActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile:
                startActivity(new Intent(context, UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.privacy_policy:
                break;
            case R.id.logout:
                LogoutAlert();
                break;
        }
    }

    private void sharepost() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Please downlaod the app from link.");
        context.startActivity(Intent.createChooser(sharingIntent, "Share Post External"));
    }


    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        savePref.clearPreferences();
                        util.showToast(context, "User Logout Successfully!");
                        Intent intent = new Intent(context, SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.clearPreferences();
                            util.showToast(context, "User Logout Successfully!");
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            util.showToast(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
