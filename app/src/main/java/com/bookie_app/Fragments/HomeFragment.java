package com.bookie_app.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bookie_app.Activities.SignInActivity;
import com.bookie_app.Adapters.BetsAdapter;
import com.bookie_app.Adapters.RequestAdapter;
import com.bookie_app.DataModels.EventBetModel;
import com.bookie_app.MainActivity;
import com.bookie_app.R;
import com.bookie_app.Util.ConnectivityReceiver;
import com.bookie_app.Util.Parameters;
import com.bookie_app.Util.SavePref;
import com.bookie_app.Util.util;
import com.bookie_app.parser.AllAPIS;
import com.bookie_app.parser.GetAsync;
import com.bookie_app.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    Context context;
    @BindView(R.id.bets)
    TextView bets;
    @BindView(R.id.requests)
    TextView requests;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_text)
    TextView errorText;
    private ArrayList<EventBetModel> list;
    RequestAdapter requestAdapter = null;
    private SavePref savePref;

    public HomeFragment() {
        // Required empty public constructor
    }


    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        if (MainActivity.new_request_bet) {
            requests.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
            bets.setTextColor(context.getResources().getColor(R.color.gray));


            if (ConnectivityReceiver.isConnected()) {
                REQUEST_BATS_LISTING_API("0");
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }


        } else {
            if (ConnectivityReceiver.isConnected()) {
                REQUEST_BATS_LISTING_API("1");
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        }


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.bets, R.id.requests})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bets:
                bets.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                requests.setTextColor(context.getResources().getColor(R.color.gray));
                if (ConnectivityReceiver.isConnected()) {
                    REQUEST_BATS_LISTING_API("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.requests:
                requests.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                bets.setTextColor(context.getResources().getColor(R.color.gray));
                if (ConnectivityReceiver.isConnected()) {
                    REQUEST_BATS_LISTING_API("0");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
        }
    }


    /*0=received request
       1=accepted request
       2=rejected request*/
    private void REQUEST_BATS_LISTING_API(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.REQUEST_BATS_LISTING + "?status=" + type, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i).getJSONObject("Bat");
                                EventBetModel eventBetModel = new EventBetModel();

                                eventBetModel.setAcceptance_end(object.getString("acceptance_end"));
                                eventBetModel.setCreated(object.getString("created"));
                                eventBetModel.setDate(object.getString("date"));
                                eventBetModel.setDate_time_type(object.getString("date_time_type"));
                                eventBetModel.setDescription(object.getString("description"));
                                eventBetModel.setId(object.getString("id"));
                                eventBetModel.setName(object.getString("name"));
                                eventBetModel.setParticipants(data.getJSONObject(i).getJSONObject("Participant").getString("id"));
                                eventBetModel.setPro_contra(object.getString("pro_contra"));
                                eventBetModel.setTime(object.getString("time"));
                                eventBetModel.setTimeframe(object.getString("timeframe"));
                                eventBetModel.setType(object.getString("type"));
                                eventBetModel.setWager(object.getString("wager"));
                                eventBetModel.setWager_type(object.getString("wager_type"));
                                eventBetModel.setWitness_need(object.getString("witness_need"));
                                list.add(eventBetModel);

                            }
                            if (list.size() > 0) {
                                myRecyclerView.setVisibility(View.VISIBLE);
                                if (type.equals("0")) {
                                    requestAdapter = new RequestAdapter(context, list, HomeFragment.this);
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    myRecyclerView.setAdapter(requestAdapter);
                                } else {
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    myRecyclerView.setAdapter(new BetsAdapter(context, list));
                                }

                                errorText.setVisibility(View.GONE);
                            } else {
                                if (type.equals("0")) {
                                    errorText.setText("No Request Found");
                                } else {
                                    errorText.setText("No Joined Bets Found");
                                }
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void REQUEST_STATUS(int position, String status, String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TYPE, type);
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        formBuilder.addFormDataPart(Parameters.PARTICIPANT_ID, list.get(position).getParticipants());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REQUEST_STATUS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            String s = "";
                            if (status.equals("1"))
                                s = "Request Accepted Sucessfully.";
                            else
                                s = "Request Declined Sucessfully.";
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(s).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    list.remove(position);
                                    requestAdapter.notifyDataSetChanged();
                                }
                            }).show();


                        } else {
                            util.showToast(context, jsonmainObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }


            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
