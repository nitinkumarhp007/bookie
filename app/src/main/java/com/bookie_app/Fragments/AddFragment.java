package com.bookie_app.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.bookie_app.Activities.AddBetActivity;
import com.bookie_app.Activities.AddEventActivity;
import com.bookie_app.Activities.ForgotPasswordActivity;
import com.bookie_app.R;
import com.bookie_app.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends Fragment {


    @BindView(R.id.add_bet)
    Button addBet;
    @BindView(R.id.add_event)
    Button addEvent;

    public AddFragment() {
        // Required empty public constructor
    }

    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        unbinder = ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        util.StoreContacts = new ArrayList<>();
        util.StoreContacts.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.add_bet, R.id.add_event})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_bet:
                startActivity(new Intent(getContext(), AddBetActivity.class));
                break;
            case R.id.add_event:
                startActivity(new Intent(getContext(), AddEventActivity.class));
                break;
        }
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
