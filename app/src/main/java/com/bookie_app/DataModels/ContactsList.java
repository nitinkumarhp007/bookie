package com.bookie_app.DataModels;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class ContactsList implements Parcelable {

    String profile="";
    String id="";
    String phone="";
    String name="";
    Bitmap bitmap;
    boolean isCheck = false;

    public ContactsList()
    {

    }

    protected ContactsList(Parcel in) {
        profile = in.readString();
        id = in.readString();
        phone = in.readString();
        name = in.readString();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
        isCheck = in.readByte() != 0;
    }

    public static final Creator<ContactsList> CREATOR = new Creator<ContactsList>() {
        @Override
        public ContactsList createFromParcel(Parcel in) {
            return new ContactsList(in);
        }

        @Override
        public ContactsList[] newArray(int size) {
            return new ContactsList[size];
        }
    };

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public String getId() {
        return id;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(profile);
        dest.writeString(id);
        dest.writeString(phone);
        dest.writeString(name);
        dest.writeParcelable(bitmap, flags);
        dest.writeByte((byte) (isCheck ? 1 : 0));
    }
}
