package com.bookie_app.DataModels;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {

    public UserModel()
    {}

    String profile="";
    String id="";
    String phone="";
    String name="";

    protected UserModel(Parcel in) {
        profile = in.readString();
        id = in.readString();
        phone = in.readString();
        name = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(profile);
        dest.writeString(id);
        dest.writeString(phone);
        dest.writeString(name);
    }
}
