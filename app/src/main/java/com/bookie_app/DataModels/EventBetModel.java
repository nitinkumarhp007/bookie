package com.bookie_app.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class EventBetModel implements Parcelable {

    public EventBetModel()
    {

    }
    String id="";
    String name="";
    String type="";
    String description="";
    String acceptance_end="";
    String pro_contra="";
    String participants="";
    String date_time_type="";
    String date="";
    String time="";
    String timeframe="";
    String witness_need="";
    String wager_type="";
    String wager="";
    String created="";
    ArrayList<UserModel>list;

    protected EventBetModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
        description = in.readString();
        acceptance_end = in.readString();
        pro_contra = in.readString();
        participants = in.readString();
        date_time_type = in.readString();
        date = in.readString();
        time = in.readString();
        timeframe = in.readString();
        witness_need = in.readString();
        wager_type = in.readString();
        wager = in.readString();
        created = in.readString();
        list = in.createTypedArrayList(UserModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(description);
        dest.writeString(acceptance_end);
        dest.writeString(pro_contra);
        dest.writeString(participants);
        dest.writeString(date_time_type);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(timeframe);
        dest.writeString(witness_need);
        dest.writeString(wager_type);
        dest.writeString(wager);
        dest.writeString(created);
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventBetModel> CREATOR = new Creator<EventBetModel>() {
        @Override
        public EventBetModel createFromParcel(Parcel in) {
            return new EventBetModel(in);
        }

        @Override
        public EventBetModel[] newArray(int size) {
            return new EventBetModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAcceptance_end() {
        return acceptance_end;
    }

    public void setAcceptance_end(String acceptance_end) {
        this.acceptance_end = acceptance_end;
    }

    public String getPro_contra() {
        return pro_contra;
    }

    public void setPro_contra(String pro_contra) {
        this.pro_contra = pro_contra;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public String getDate_time_type() {
        return date_time_type;
    }

    public void setDate_time_type(String date_time_type) {
        this.date_time_type = date_time_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    public String getWitness_need() {
        return witness_need;
    }

    public void setWitness_need(String witness_need) {
        this.witness_need = witness_need;
    }

    public String getWager_type() {
        return wager_type;
    }

    public void setWager_type(String wager_type) {
        this.wager_type = wager_type;
    }

    public String getWager() {
        return wager;
    }

    public void setWager(String wager) {
        this.wager = wager;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public ArrayList<UserModel> getList() {
        return list;
    }

    public void setList(ArrayList<UserModel> list) {
        this.list = list;
    }
}
