package com.bookie_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bookie_app.Fragments.AddFragment;
import com.bookie_app.Fragments.HomeFragment;
import com.bookie_app.Fragments.NotificationsFragment;
import com.bookie_app.Fragments.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.title)
    TextView title;
    public static Toolbar toolbar;
    public static MainActivity context;

    public static boolean new_request_bet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        title.setText("Home");

        new_request_bet=getIntent().getBooleanExtra("new_request_bet",false);

        loadFragment(new HomeFragment());
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_event:
                    title.setText("Home");
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_class:
                    title.setText("Add");
                    loadFragment(new AddFragment());
                    return true;
                case R.id.navigation_chat:
                    title.setText("Notification");
                    loadFragment(new NotificationsFragment());
                    return true;
                case R.id.navigation_notification:
                    title.setText("Profile");
                    loadFragment(new ProfileFragment());
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);


        transaction.commit();
    }
}

